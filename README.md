# React.js CRUD App with Vue Router & Axios

Code is based on this example: https://github.com/bezkoder/vue-js-client-crud

Start app first: https://gitlab.com/spring-utils/spring-mysql

### Set port
.env

```bash
PORT=8081
```

## Project setup

In the project directory, you can run:

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm start
```
